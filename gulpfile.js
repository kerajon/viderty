var gulp                  = require('gulp'),
    less 		              = require('gulp-less'),
    minifycss 	          = require('gulp-minify-css'),
    concat 		            = require('gulp-concat'),
    cache 		            = require('gulp-cache'),
	  sourcemaps 	          = require('gulp-sourcemaps'),
	  uglify 		            = require('gulp-uglify'),
    watch 		            = require('gulp-watch'),
	  cache 		            = require('gulp-cached'),
	  autoprefixer          = require('gulp-autoprefixer'),
    htmlmin               = require('gulp-htmlmin'),
    templateToString      = require('gulp-angular-templatecache');

var gutil = {
  log: function (error) {
    console.log('\n'+ error +'\n');
  }
};

//  LESS to CSS to SINGLE FILE to COMPRESSED
gulp.task('less', function() {

  return gulp
    .src(['src/assets/less/**/*.less','assets/less/**/*.css'])
    .pipe(less())
    .pipe(autoprefixer({browser: ['last 2 version, > 10%'], cascade: false}))
    .pipe(concat("style.css"))
    .pipe(minifycss())
    .pipe(gulp.dest('dist/assets/css/'));

});
//  JS to SINGLE FILE to COMPRESSED + MAP GENAROR
gulp.task('js', function() {
  //  vendors go first - PROVIDE NAME FILES IN ORDER     .pipe(uglify({mangle: false}))
  var vendors_list = [
    'src/assets/js/vendors/fastclick.js',
    'src/assets/js/vendors/angular.min.js',
    'src/assets/js/vendors/angular-ui-router.min.js',    
    'src/assets/js/vendors/angular-touch.min.js'
  ];
  //  after vendors - EVERYELSE JS FILE
  return gulp
    .src(vendors_list.concat(['src/assets/js/**/*.js']))
    .pipe(sourcemaps.init())
    .pipe(concat("app.js"))
    .pipe(sourcemaps.write('_debug/'))
    .pipe(gulp.dest('dist/assets/js/'));

});
//  CSS to SINGLE FILE to COMPRESSED
gulp.task('css', function() {

  return gulp
    .src(['src/assets/css/*.css'])
    .pipe(cache('css'))
    .pipe(concat("style_common.css"))
    .pipe(gulp.dest('dist/assets/css/'));
        
});

gulp.task('font', function () {
  
  return gulp
    .src(['src/assets/fonts/*.otf', 'src/assets/font/*.svg', 'src/assets/font/*.ttf', 'src/assets/font/*.woff', 'src/assets/font/*.woff2', 'src/assets/font/*.eot'])
    .pipe(cache('font'))
    .pipe(gulp.dest('dist/assets/fonts/'));

});
//  SHOW COMMAND
gulp.task('default', function () {
  print_JARO();
  return console.log('\n:::::::::::::::::::::::::::\n: Use command: gulp start :\n:::::::::::::::::::::::::::\n');

});
//  COPY HTML FILES
gulp.task('html', function () {

  gulp.src(['./src/**/*.html', 'src/partials/*.html']).pipe(gulp.dest('./dist'));
  gulp.src(['./src/partials/*.html']).pipe(gulp.dest('./dist/partials')); 

});

gulp.task('watch', function () {
  
  try {
    gulp.watch('src/**/*.html', ['html']);
  } catch (e) {
    console.log('\n'+ e +'\n');
  }
  try {
    gulp.watch('src/assets/less/**', ['less']);
  } catch (e) {
    console.log('\n'+ e +'\n');
  }
  try {
    gulp.watch('src/assets/css/**', ['css']);
  } catch (e) {
    console.log('\n'+ e +'\n');
  }
  try {
    gulp.watch('src/assets/font/**', ['font']);
  } catch (e) {
    console.log('\n'+ e +'\n');
  }
  try {
    gulp.watch('src/assets/js/**', ['js']).on('error', gutil.log);
  } catch (e) {
    console.log('\n'+ e +'\n');
  }

});

gulp.task('start',['js', 'html', 'less', 'css', 'font'], function(){
  
  print_JARO('watch');
	gulp.start('watch');

});

//  gulp-less                     + 
//  gulp-concat                   + 
//  gulp-minify-css               +
//  gulp-uglify                   +
//  gulp-cache                    +
//  gulp-watch                    +
//  gulp-sourcemaps               +
//  gulp-htmlmin                  +
//  gulp-angular-templatecache    +
//  gulp-autoprefixer             +

//  for mobile - angular deps
//
//  

//  helper functions
function print_JARO (param) {

  console.log('\n\n');
  console.log('      :::::::::::::        ::::::::::::::       :::::::::::::::        :::::::::::::: ');
  console.log('                ::::      ::::        ::::      ::::        ::::      ::::        ::::');
  console.log('                ::::      ::::        ::::      ::::         ::::     ::::        ::::');
  console.log('                ::::      ::::        ::::      ::::         ::::     ::::        ::::');
  console.log('                ::::      ::::        ::::      ::::        ::::      ::::        ::::');
  console.log('                ::::      ::::::::::::::::      ::::::::::::::::      ::::        ::::');
  console.log('               ::::       ::::        ::::      ::::         ::::     ::::        ::::');
  console.log('              ::::        ::::        ::::      ::::          ::::    ::::        ::::');
  console.log('             ::::         ::::        ::::      ::::          ::::    ::::        ::::');
  console.log('           ::::           ::::        ::::      ::::          ::::    ::::        ::::');
  console.log('         ::::             ::::        ::::      ::::          ::::     :::::::::::::: ');
  console.log('\n\n');
  if (param === 'watch') {
  console.log('      ::::::::::::::::::::::::::::::::   WATCHING   ::::::::::::::::::::::::::::::::::\n\n');
  }

}