(function (window, angular) {
  'use strict';

  var Support = angular.module('Support', []);

  Support.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

    $stateProvider.state('tickets-list', {
      parent: 'viderty',
      title: "Support",
      url: "/support/tickets/list",
      templateUrl: "../partials/tickets.html",
      controller: "IssuesListCtrl",
      data: {
        authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
        pageHead: {title: 'Support', shortDescription: 'support tickets and requests'}
      }
    });

    $stateProvider.state('tickets-create', {
      parent: 'viderty',
      title: "Support",
      url: "/support/tickets/create",
      templateUrl: "../partials/create-tickets.html",
      controller: "IssuesCreateCtrl",
      data: {
        authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
        pageHead: {title: 'Support', shortDescription: 'support tickets and requests'}
      }
    });

  });

  Support.controller('IssuesListCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

  Support.controller('IssuesCreateCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

})(window, angular);