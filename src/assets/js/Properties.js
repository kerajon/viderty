(function (window, angular) {
  'use strict';

  var Properties = angular.module('Properties', []);

  Properties.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

      $stateProvider.state('properties-create', {
        parent: 'viderty',
        title: "Properties",
        url: "/properties/create",
        templateUrl: "../partials/create-properties.html",
        controller: "PropertiesCreateCtrl",
        data: {
          authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
          pageHead: {title: 'Properties', shortDescription: 'create a new property'}
        }
      });

      $stateProvider.state('properties-list', {
        parent: 'viderty',
        title: "Properties",
        url: "/properties",
        templateUrl: "../partials/properties.html",
        controller: "PropertiesListCtrl",
        data: {
          authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
          pageHead: {title: 'Properties', shortDescription: 'create, search and view properties'}
        }
      });

      $stateProvider.state('properties-search', {
        parent: 'viderty',
        title: "Properties",
        url: "/properties/search",
        templateUrl: "../partials/search-properties.html",
        controller: "PropertiesSearchCtrl",
        data: {
          authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
          pageHead: {title: 'Properties', shortDescription: 'create, search and view properties'}
        }
      });

  });

  Properties.controller('PropertiesCreateCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

  Properties.controller('PropertiesListCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

  Properties.controller('PropertiesSearchCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

})(window, angular);