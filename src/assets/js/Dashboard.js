(function (window, angular) {
  'use strict';

  var Dashboard = angular.module( "Dashboard", ['Debug']);

  Dashboard.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

      $stateProvider.state('dashboard', {
        parent: 'viderty',
        title: "Dashboard",
        url: "/dashboard",
        templateUrl: "../partials/dashboard.html",
        controller: "Dashboard",
        data: {
          authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
          pageHead: {title: 'Dashboard', shortDescription: 'statistics and more'}
        }
      });

  });

  Dashboard.controller("Dashboard", function($scope, debug, $state, Page) {

    debug.log('Dashboard', 'Dashboard controller loaded');

    Page.setHeader($scope, $state);

  });

})(window, angular);