(function (window, angular) {
  'use strict';

  var Rest = angular.module('Rest', ['Debug']);

  Rest.service('Server', function ($q, $http) {

    var url, token, server_url;

    var Server = {

      setToken: function (token) {
        token = token;
        url = server_url +'/'+ access_token +'/';
      },

      setServer: function (server_url) {
        server_url = server_url;
        url = server_url +'/';
      },

      post: function (params) {
        var deffered = $q.defer();

        if (!angular.isObject(params)) throw new Error('Module Server: Params not Object');

        $http({
          method: 'POST',
          url: url,
          data: params
        }).then(function (res) {

          deffered.resolve(res.data);

        }, function (err) {

          deffered.reject();
          
        });

        return deffered.promise;
      },

      get: function (params) {
        var deffered = $q.defer();

        if (!angular.isObject(params)) throw new Error('Module Server: Params not Object');

        $http({
          method: 'GET',
          url: url
        }).then(function (res) {

          deffered.resolve(res.data);

        }, function (err) {

          deffered.reject();
          
        });

        return deffered.promise;
      },

      update: function (params) {
        var deffered = $q.defer();

        if (!angular.isObject(params)) throw new Error('Module Server: Params not Object');

        $http({
          method: 'PUT',
          url: url,
          data: params || {}
        }).then(function (res) {

          deffered.resolve(res.data);

        }, function (err) {

          deffered.reject();
          
        });

        return deffered.promise;
      },

      remove: function (params) {
        var deffered = $q.defer();

        if (!angular.isObject(params)) throw new Error('Module Server: Params not Object');

        $http({
          method: 'DELETE',
          url: url,
          data: params || {}
        }).then(function (res) {

          deffered.resolve(res.data);

        }, function (err) {

          deffered.reject();
          
        });

        return deffered.promise;
      },

      simulate: function (params) {
        var deffered = $q.defer();

        if (!angular.isObject(params)) throw new Error('Module Server: Params not Object');
        var url_computed;
        switch (params.type) {
          case 'login':
            url_computed = url +'server_simulation/'+ params.type +'.json';
            break;
        
          default:
            break;
        }

        $http({
          method: 'GET',
          url: url_computed
        }).then(function (res) {

          deffered.resolve(res.data);

        }, function (err) {

          deffered.reject();
          
        });

        return deffered.promise;
      }

    };

    return Server;
  });

  Rest.service('Api', function (debug, $q, $http, Server) {

    debug.log('Rest', 'Api_service_loaded');

    Server.setServer('http://127.0.0.1:8080/');

    var Api = {

      simulate_login: function (credentials) {
        var deffered = $q.defer();

        credentials.type = 'login';
        Server
        .simulate(credentials)
        .then(function (res) {
          var data = {
            userRole: res[credentials.email] ? res[credentials.email].role : '',
            userId: res[credentials.email] ? credentials.email : '',
            userDetails: {
              firstname: res[credentials.email] ? res[credentials.email].firstname : '',
              lastname: res[credentials.email] ? res[credentials.email].lastname : ''
            }
          };
          deffered.resolve(data)
        }, function (err) {
          deffered.reject();
        });

        return deffered.promise;
      }
      
    };

    return Api;
  });


})(window, angular);