(function (window, angular) {
  'use strict';

  var User = angular.module('User', []);

  User.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

    $stateProvider.state('account-edit', {
      parent: 'viderty',
      title: "User",
      url: "/account/edit",
      templateUrl: "../partials/edit-profile-user.html",
      controller: "AccountEditCtrl",
      data: {
        authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.trial, USER_ROLES.normal],
        pageHead: {title: 'My Account', shortDescription: 'edit my details, change password'}
      }
    });

    $stateProvider.state('account-company-edit', {
      parent: 'viderty',
      title: "User",
      url: "/company/edit",
      templateUrl: "../partials/edit-profile-company.html",
      controller: "CompanyEditCtrl",
      data: {
        authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.trial, USER_ROLES.normal],
        pageHead: {title: 'Company Details', shortDescription: 'edit company details'}
      }
    });

  });

  User.controller('AccountEditCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

  User.controller('CompanyEditCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

})(window, angular);