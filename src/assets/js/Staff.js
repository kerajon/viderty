(function (window, angular) {
  'use strict';

  var Staff = angular.module('Staff', []);

  Staff.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

    $stateProvider.state('staff-list', {
      parent: 'viderty',
      title: "Staff",
      url: "/staff/list",
      templateUrl: "../partials/staff.html",
      controller: "StaffListCtrl",
      data: {
        authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
        pageHead: {title: 'Staff', shortDescription: 'view and edit staff members'}
      }
    });

    $stateProvider.state('staff-create', {
      parent: 'viderty',
      title: "Staff",
      url: "/staff/create",
      templateUrl: "../partials/create-staff.html",
      controller: "StaffCreateCtrl",
      data: {
        authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
        pageHead: {title: 'Staff', shortDescription: 'create a staff member'}
      }
    });

  });

  Staff.controller('StaffListCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

  Staff.controller('StaffCreateCtrl', function ($scope, $state, Page) {

    Page.setHeader($scope, $state);

  });

})(window, angular);