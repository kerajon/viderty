(function (window, angular) {
  'use strict';

  var Phrases = angular.module('Phrases', []);

  Phrases.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

      $stateProvider.state('phrases-list', {
        parent: 'viderty',
        title: "Phrases",
        url: "/phrases",
        templateUrl: "../partials/phrases.html",
        controller: "PhrasesListCtrl",
        data: {
          authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
          pageHead: {title: 'Phrases', shortDescription: 'view and edit your phrases'}
        }
      });

      $stateProvider.state('phrases-create', {
        parent: 'viderty',
        title: "Properties",
        url: "/phrases/create",
        templateUrl: "../partials/create-phrases.html",
        controller: "PhrasesCreateCtrl",
        data: {
          authRoles: [USER_ROLES.payed, USER_ROLES.admin, USER_ROLES.demo, USER_ROLES.trial, USER_ROLES.normal],
          pageHead: {title: 'Phrases', shortDescription: 'create a phrase'}
        }
      });

  });

  Phrases.controller("PhrasesListCtrl", function($scope, debug, $state, Page) {

    debug.log('Phrases', 'Phrases List Controller');

    Page.setHeader($scope, $state);

  });

  Phrases.controller("PhrasesCreateCtrl", function($scope, debug, $state, Page) {

    debug.log('Phrases', 'Phrases Create Controller');

    Page.setHeader($scope, $state);

  });

})(window, angular);