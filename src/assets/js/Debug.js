(function (window, angular) {
  'use strict';

  var Debug = angular.module('Debug', []);

  Debug.service('debug', function () {
    return {
      enable: false,
      log: function (module, data) {
        if (!this.enable) return;
        console.log(data);
      }
    };
  });

})(window, angular);