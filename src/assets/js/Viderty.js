(function (window, angular) {
  'use strict';

  var Viderty = angular.module('Viderty', ['ui.router', 'ngTouch', 'Auth', 'Rest', 'Debug', 'Dashboard', 'Properties', 'Phrases', 'Support', 'Staff', 'User']);

  //  main route configuration
  Viderty.config(function($stateProvider, $urlRouterProvider, USER_ROLES) {

    $urlRouterProvider.otherwise("/");

    $stateProvider

      .state('viderty', {
        url: "/viderty",
        title: "Viderty",
        templateUrl: "../partials/layout.html",
        controller: 'VidertyLayoutCtl',
        redirectTo: "viderty.dashboard",
        data: {
          authRoles: [USER_ROLES.all]
        }
      });

  });

  //  enable debug mode
  Viderty.run(function($rootScope, $state, debug) {

    debug.enable = true;
    if (debug.enable) console.log('Debug enabled');

  });

  Viderty.factory('Page', function (UserSession) {
    var Page = {};

    Page.setHeader = function (scope, state) {

      scope.pageHead = {
        title: state.current.data.pageHead.title,
        shortDescription: state.current.data.pageHead.shortDescription,
        isTrial: UserSession.userRole === 'trial' ? true : false
      };

    };

    return Page;
  });

  Viderty.controller('VidertyLayoutCtl', function ($scope, debug, UserSession) {

    $scope.dropDown = {};
    $scope.user = {
      firstname: UserSession.firstname,
      lastname: UserSession.lastname
    };

    (function ($scope) {
      var littleMenu = $scope.dropDown.littleMenu = {};
      littleMenu.opened = false;
      littleMenu.open = function () {
        littleMenu.opened = !littleMenu.opened;
      };
    })($scope);

    (function ($scope) {
      var mainMenu = $scope.dropDown.mainMenuSupport = {};
      mainMenu.opened = false;
      mainMenu.open = function () {
        mainMenu.opened = !mainMenu.opened;
      };
    })($scope);

    (function ($scope) {
      var mainMenu = $scope.dropDown.mainMenuStaff = {};
      mainMenu.opened = false;
      mainMenu.open = function () {
        mainMenu.opened = !mainMenu.opened;
      };
    })($scope);

    $scope.signOut = function () {
      UserSession.destroy();
    };

  });

})(window, angular);